#!/bin/bash

# v1.0.0

# Get script dir
gsd=`dirname $0`

# Create backup for either web server dir OR db OR both
bkdrbool=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /bkdrbool/) print $2}' $gsd"/"params.ini)
dbbool=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /dbbool/) print $2}' $gsd"/"params.ini)

# Files will be backed up in this dorectory, change it in params.ini file
stgdir=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /stgdir/) print $2}' $gsd"/"params.ini)

# Backup database
if [ $dbbool == TRUE ]; then

	# DB username and password, update them in params.ini file
	dbusername=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /dbusername/) print $2}' $gsd"/"params.ini)
	dbpassword=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /dbpassword/) print $2}' $gsd"/"params.ini)

	# DB name, update it in params.ini file
	dbname=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /dbname/) print $2}' $gsd"/"params.ini)

	# DB name with date & time
	dbvar=$dbname-$(date +%Y%m%d%H%M%S)

	# MySQL dump 
	if mysqldump -u$dbusername -p$dbpassword --opt $dbname > $dbvar.sql; then

		# Output backup file name
		outputfile=$stgdir$dbvar.tgz
		
		#Display message about starting the backup
		echo "Starting backup of database "$dbname" into "$stgdir" with file name "$dbvar.tgz

		# Create tgz
		if tar -zcvf $outputfile $dbvar.sql 2>/dev/null; then

			# Print successful message and remove *.sql file
			echo "Backup created successfully for database: "$dbname
			rm $dbvar.sql
		else

			# Print error, if some thing goes wrong
			echo "Error occurred while creating backup for database: "$dbname
		fi
	else
		echo "Error occured while performing database backup, please make sure you can connect to MySQL server. Check the login credintials and dbname in params.ini. Skipping..."
	fi	
	
else
	echo "Database backup is disabled in params.ini file with FALSE. Skipping..."
fi

# Backup dir
if [ $bkdrbool == TRUE ]; then

	# Dir to backup
	bakdir=$(awk -F '=' '{if (! ($0 ~ /^;/) && $0 ~ /bakdir/) print $2}' $gsd"/"params.ini)

	# Output backup file name
	filename=backup-$(date +%Y%m%d%H%M%S).tgz
	
	# Output backup file name with extension
	outputfile=$stgdir$filename
	
	#Display message about starting the backup
	echo "Starting backup of directory "$bakdir" into "$stgdir" with file name "$filename
	
	#Create tar.gz
	if tar -zczf $outputfile $bakdir 2>/dev/null;  then

		#Display success message
		echo "The file: "$filename" was created as backup for: "$bakdir
	else

		#Display error message
		echo "There was a problem creating: "$filename" as a backup for: "$bakdir" in "$bakdir
	fi
else
	echo "Directory backup is disabled in params.ini file with FALSE. Skipping..."
fi
