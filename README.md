#### Bash script to create backup of web server or any desired directory and mysql database
Feel free to distribute, modify and use.  

#### Download
Go to Release page and download the latest version.

#### How to use - params.ini
Open params.ini file and update the params accordingly.
```
bkdrbool=FALSE
dbbool=TRUE
dbusername=petstore
dbpassword=TEST 
dbname=test
stgdir=/backup/
bakdir=/var/www/test_project
```
Line 1: set it to true for backing up web server directory or any other directory, TRUE = create, FALSE = skip  
Line 2: set it to true for database backup, TRUE = create, FALSE = skip  
Line 3: Database username  
Line 4: Database password  
Line 5: Database Name  
Line 6: Storage/target directory, where you want to backup your files  
Line 7: Directory to create backup of. e.g: website directory.  

#### Cron setup
This will run the script every day at 11PM with a log file created to output log events. Change the path of the script and log file.  

```
0 23 * * * /backup/bash/ezi_bakup.sh >> /backup/bash/cron.log 2>&1
```